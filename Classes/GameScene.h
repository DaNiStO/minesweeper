#ifndef __GAMESCENE_H__
#define __GAMESCENE_H__

#include <Gestures/LongPressGestureRecognizer.h>
#include <Gestures/TapGestureRecognizer.h>
#include "cocos2d.h"
USING_NS_CC;
using namespace std;

const int GRID_WIDTH = 5;
const int GRID_HEIGHT = 5;
const int MAP_WIDTH = 20;
const int MAP_HEIGHT = 20;
const int GRID_SIZE = 128;
const int MINES_NUMBER = 10;

enum class PRESS_TYPE {
    TAP_PRESS,
    LONG_PRESS,
};

enum class GRID_REAL_TYPE {
    NUM,
    MINE
};

enum class GRID_FACE_TYPE {
    NOTHING,
    NUM,
    FLAG
};

typedef struct {
    int number;
    GRID_REAL_TYPE realType;
    GRID_FACE_TYPE faceType;
    Sprite *sp;
} GRID;

typedef struct {
    int x;
    int y;
} MyPoint;

class GameScene : public Layer {
private:
    int minesNums;
    vector<MyPoint> minesPosition;
    vector<MyPoint> wrongFlag;
    GRID map[MAP_WIDTH + 2][MAP_HEIGHT + 2];

    bool isClicked;
    int tap_x, long_x;
    int tap_y, long_y;
    PRESS_TYPE press_type;
    Size size;

    // Gestures
    LongPressGestureRecognizer* onLongPressListener;
    TapGestureRecognizer* onTapPressListener;

    void singleClicked(int x, int y);

    void doubleClicked(int x, int y);

    void setMines();

    void setNumber();

    void showNumber(int x, int y, int n);

    void showBlank(int x, int y);

    void showFlag(int x, int y);

    void showAround(int x, int y);

    void copyWithAround(int x, int y);

    int calcFlagNumber(int x, int y);

    bool isWin();

    void gameOver(string str);

    void gameWin();

    void onGestureRecognizer(GestureRecognizer* recognizer, PRESS_TYPE type);

    void onTapPress(TapGestureRecognizer* recognizer);

    void onLongPress(LongPressGestureRecognizer* recognizer);

    void removeCallbacks();

public:
    static Scene *createScene();

    virtual bool init();

    void newGameCallback(Ref *obj);

    CREATE_FUNC(GameScene);
};
#endif