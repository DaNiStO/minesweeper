#include "GameScene.h"

USING_NS_CC;

int scale;

Scene *GameScene::createScene() {
    scale = 2;

    auto scene = Scene::create();
    auto layer = GameScene::create();
    scene->addChild(layer);
    return scene;
}

bool GameScene::init() {
    if (!Layer::init())
        return false;

    size = Director::getInstance()->getWinSize();

    minesNums = MINES_NUMBER;
    isClicked = false;

    for (int i = 0; i <= GRID_WIDTH * scale + 1; ++i)
        for (int j = 0; j <= GRID_HEIGHT * scale + 1; ++j) {
            map[i][j].realType = GRID_REAL_TYPE::NUM;
            map[i][j].faceType = GRID_FACE_TYPE::NOTHING;
            map[i][j].number = 0;
        }

    for (int i = 1; i <= GRID_WIDTH * scale; ++i)
        for (int j = 1; j <= GRID_HEIGHT * scale; ++j) {
            auto sp = Sprite::create("cell_unexposed_empty.png");
            sp->setAnchorPoint(Vec2(1, 1));
            sp->setScale(1.0 / scale);
            sp->setPosition(Vec2(GRID_SIZE * i / scale, GRID_SIZE * j / scale));
            addChild(sp);
            map[i][j].sp = sp;
        }

    setMines();
    setNumber();

    auto menuItemNewGame = MenuItemFont::create("New Game",
                                                CC_CALLBACK_1(GameScene::newGameCallback, this));
    menuItemNewGame->setColor(Color3B::WHITE);
    menuItemNewGame->setAnchorPoint(Vec2(1, 0.5));

    auto menu = Menu::create(menuItemNewGame, NULL);
    menu->setPosition(Vec2::ZERO);
    menuItemNewGame->setPosition(Vec2(size.width * 0.94, size.height * 0.8));
    addChild(menu);

    auto author = Label::createWithSystemFont("Develop By Daniel Stolero", "Roboto", 85);
    author->setPosition(Vec2(size.width * 0.85, size.height * 0.6));
    author->setScale(0.3);
    this->addChild(author);

    onLongPressListener = LongPressGestureRecognizer::create();
    onLongPressListener->onLongPress = CC_CALLBACK_1(GameScene::onLongPress, this);
    addChild(onLongPressListener);

    onTapPressListener = TapGestureRecognizer::create();
    onTapPressListener->onTap = CC_CALLBACK_1(GameScene::onTapPress, this);
    addChild(onTapPressListener);

    return true;
}

void GameScene::onTapPress(TapGestureRecognizer *recognizer) {
    onGestureRecognizer(recognizer, PRESS_TYPE::TAP_PRESS);
}

void GameScene::onLongPress(LongPressGestureRecognizer *recognizer) {
    onGestureRecognizer(recognizer, PRESS_TYPE::LONG_PRESS);
}

void GameScene::onGestureRecognizer(GestureRecognizer *recognizer, PRESS_TYPE type) {
    float x = recognizer->getGestureLocation().x;
    float y = recognizer->getGestureLocation().y;

    if ((x == 0 && y == 0) ||
        (x > GRID_SIZE * GRID_WIDTH))
        return;

    else if(type == PRESS_TYPE::TAP_PRESS && tap_x == (int) x * scale / GRID_SIZE + 1 && tap_y == (int) y * scale / GRID_SIZE + 1)
        return;

    else if(type == PRESS_TYPE::LONG_PRESS && long_x == (int) x * scale / GRID_SIZE + 1 && long_y == (int) y * scale / GRID_SIZE + 1)
        return;

    printf("on press at (%f,%f)\n", x, y);

    if(type == PRESS_TYPE::TAP_PRESS) {
        tap_x = (int) x * scale / GRID_SIZE + 1;
        tap_y = (int) y * scale / GRID_SIZE + 1;
    } else if(type == PRESS_TYPE::LONG_PRESS) {
        long_x = (int) x * scale / GRID_SIZE + 1;
        long_y = (int) y * scale / GRID_SIZE + 1;
    }

    int row = (int) x * scale / GRID_SIZE + 1;
    int column = (int) y * scale / GRID_SIZE + 1;

    printf("on press at row: %d | column: %d\n", row, column);

    if (isClicked) {
        isClicked = false;
        doubleClicked(row, column);
    } else {
        isClicked = true;
        press_type = type;

        singleClicked(row, column);
    }
}

void GameScene::singleClicked(int x, int y) {
    if (isClicked)
        isClicked = false;

    if (map[x][y].faceType != GRID_FACE_TYPE::NUM) {
        if (press_type == PRESS_TYPE::TAP_PRESS) {
            if (map[x][y].realType == GRID_REAL_TYPE::MINE) {
                gameOver("Game Over!");

                auto sp = Sprite::create("x.png");
                sp->setAnchorPoint(Vec2(1, 1));
                sp->setScale(1.0 / scale);
                sp->setPosition(Vec2(GRID_SIZE * x / scale, GRID_SIZE * y / scale));
                addChild(sp, 2);
            } else if (map[x][y].realType == GRID_REAL_TYPE::NUM && map[x][y].number != 0) {
                showNumber(x, y, map[x][y].number);
            } else {
                showBlank(x, y);
            }
        } else if (press_type == PRESS_TYPE::LONG_PRESS) {
            if (map[x][y].faceType == GRID_FACE_TYPE::NOTHING) {
                map[x][y].faceType = GRID_FACE_TYPE::FLAG;
                showFlag(x, y);

                minesNums--;
            } else if (map[x][y].faceType == GRID_FACE_TYPE::FLAG) {
                map[x][y].faceType = GRID_FACE_TYPE::NOTHING;
                map[x][y].sp->removeFromParent();

                minesNums++;
            }
        }

        if (minesNums == 0 && isWin())
            gameWin();
    }
}

void GameScene::doubleClicked(int x, int y) {
    if (map[x][y].faceType == GRID_FACE_TYPE::NOTHING) {
        press_type = PRESS_TYPE::TAP_PRESS;
        singleClicked(x, y);
        return;
    } else if (map[x][y].faceType == GRID_FACE_TYPE::FLAG)
        return;

    if (map[x][y].number != 0 && calcFlagNumber(x, y) == map[x][y].number) {
        showAround(x, y);
    }

    if (minesNums == 0 && isWin())
        gameWin();
}

void GameScene::setMines() {
    srand((unsigned) time(NULL));
    int x, y;

    for (int i = 0; i != minesNums;) {
        x = rand() % (GRID_WIDTH * scale) + 1;
        y = rand() % (GRID_WIDTH * scale) + 1;

        if (map[x][y].realType != GRID_REAL_TYPE::MINE) {
            map[x][y].realType = GRID_REAL_TYPE::MINE;
            i++;
            minesPosition.push_back(MyPoint{x, y});
        }
    }
}

void GameScene::setNumber() {
    int number = 0;

    for (int i = 1; i <= GRID_WIDTH * scale; ++i)
        for (int j = 1; j <= GRID_HEIGHT * scale; ++j) {
            number = 0;

            if (map[i][j].realType != GRID_REAL_TYPE::MINE) {
                for (int m = -1; m <= 1; ++m)
                    for (int n = -1; n <= 1; ++n)
                        if (!(m == 0 && n == 0) &&
                            map[i + m][j + n].realType == GRID_REAL_TYPE::MINE)
                            number++;

                map[i][j].number = number;
            }
        }
}

void GameScene::showNumber(int x, int y, int n) {
    map[x][y].faceType = GRID_FACE_TYPE::NUM;

    auto sp = Sprite::create(StringUtils::format("cell_exposed_%d.png", n));
    sp->setAnchorPoint(Vec2(1, 1));
    sp->setScale(1.0 / scale);
    sp->setPosition(Vec2(GRID_SIZE * x / scale, GRID_SIZE * y / scale));
    addChild(sp);
    map[x][y].sp = sp;
}

void GameScene::showBlank(int x, int y) {
    if (x < 1 || x > GRID_WIDTH * scale || y < 1 || y > GRID_HEIGHT * scale)
        return;

    if (map[x][y].realType == GRID_REAL_TYPE::MINE)
        return;

    if (map[x][y].faceType == GRID_FACE_TYPE::FLAG)
        return;

    if (map[x][y].faceType == GRID_FACE_TYPE::NUM)
        return;

    showNumber(x, y, map[x][y].number);

    if (map[x][y].number != 0)
        return;

    for (int m = -1; m <= 1; ++m)
        for (int n = -1; n <= 1; ++n)
            if (!(m == 0 && n == 0))
                showBlank(x + m, y + n);
}

void GameScene::showFlag(int x, int y) {
    auto sp = Sprite::create("cell_unexposed_flag.png");
    sp->setAnchorPoint(Vec2(1, 1));
    sp->setScale(1.0 / scale);
    sp->setPosition(Vec2(GRID_SIZE * x / scale, GRID_SIZE * y / scale));
    addChild(sp);
    map[x][y].sp = sp;
}

void GameScene::showAround(int x, int y) {
    for (int i = -1; i <= 1; ++i)
        for (int j = -1; j <= 1; ++j) {
            if (!(i == 0 && j == 0) && (x + i) >= 1 && (x + i) <= GRID_WIDTH * scale &&
                (y + j) >= 1 && (y + j) <= GRID_HEIGHT * scale)
                copyWithAround(x + i, y + j);
        }

    if (wrongFlag.size() != 0) {
        gameOver("Bomb!");
        wrongFlag.clear();
    }
}

void GameScene::copyWithAround(int x, int y) {
    if (map[x][y].faceType == GRID_FACE_TYPE::NOTHING &&
        map[x][y].realType == GRID_REAL_TYPE::NUM) {
        if (map[x][y].number != 0)
            showNumber(x, y, map[x][y].number);
        else
            showBlank(x, y);
    } else if (map[x][y].faceType == GRID_FACE_TYPE::FLAG &&
               map[x][y].realType != GRID_REAL_TYPE::MINE)
        wrongFlag.push_back(MyPoint{x, y});
}

int GameScene::calcFlagNumber(int x, int y) {
    int number = 0;

    for (int m = -1; m <= 1; ++m)
        for (int n = -1; n <= 1; ++n)
            if (!(m == 0 && n == 0) && map[x + m][y + n].faceType == GRID_FACE_TYPE::FLAG)
                number++;

    return number;
}

bool GameScene::isWin() {
    for (int i = 1; i <= GRID_WIDTH * scale; ++i)
        for (int j = 1; j <= GRID_HEIGHT * scale; ++j) {
            if (map[i][j].faceType == GRID_FACE_TYPE::NOTHING)
                return false;
            else if (map[i][j].faceType == GRID_FACE_TYPE::FLAG &&
                     map[i][j].realType != GRID_REAL_TYPE::MINE)
                return false;
        }

    return true;
}

void GameScene::gameOver(string str) {
    removeCallbacks();

    for (auto p : minesPosition) {
        auto sp = Sprite::create("cell_exposed_bomb.png");
        sp->setAnchorPoint(Vec2(1, 1));
        sp->setScale(1.0 / scale);
        sp->setPosition(Vec2(GRID_SIZE * p.x / scale, GRID_SIZE * p.y / scale));
        addChild(sp);
        map[p.x][p.y].sp = sp;
    }

    for (auto p : wrongFlag) {
        auto sp = Sprite::create("x.png");
        sp->setAnchorPoint(Vec2(1, 1));
        sp->setScale(1.0 / scale);
        sp->setPosition(Vec2(GRID_SIZE * p.x / scale, GRID_SIZE * p.y / scale));
        addChild(sp, 2);
    }

    auto gameOverLabel = Label::createWithSystemFont(str, "Roboto", 100);
    gameOverLabel->setPosition(size / 2);
    addChild(gameOverLabel, 3);
}

void GameScene::gameWin() {
    removeCallbacks();

    auto gameWinLabel = Label::createWithSystemFont("You Win!", "Roboto", 100);
    gameWinLabel->setPosition(size / 2);
    addChild(gameWinLabel, 3);
}

void GameScene::newGameCallback(Ref *obj) {
    removeCallbacks();

    auto scene = GameScene::createScene();
    Director::getInstance()->replaceScene(scene);
}

void GameScene::removeCallbacks() {
    _eventDispatcher->removeEventListenersForTarget(onLongPressListener);
    _eventDispatcher->removeEventListenersForTarget(onTapPressListener);
}